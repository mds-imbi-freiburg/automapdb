from automapdb import AutoMapDB, TableManager, TableMapper

# Create AutoMapDB
connection_string = "postgresql://i2b2:demouser@localhost:5432/i2b2"
db = AutoMapDB(connection_string)

# Create mapper for Tables
mapper = TableMapper(db)

# Create mappings for all tables in schema 'i2b2pm'
mapper.add_all("i2b2pm")

# Change table_mapping mapping_name
mapper.update("i2b2pm.pm_project_data", name="project")

# Create TableManager object
table = TableManager(db, mapper=mapper)

# List rows in Table project (=i2b2pm.pm_project_data)
projects = table.list("project", fields=["project_id", "project_path", "status_cd"])


# Iterate over rows
for project in projects:
    print(project)

    # Get row data
    if project["status_cd"] == "D":

        # Update data in row
        table.update(
            "project", project["project_id"], project["project_path"], "status_cd", "A"
        )


# Add mapper for Table
mapper.add_all("i2b2demodata")

# Create mapping_name to table
mapper.update("i2b2demodata.observation_fact", name="of")

# List table content with an ILIKE filter on the field "observation_blob"
result = table.list(
    "of",
    fields=["encounter_num", "observation_blob"],
    ilike={"observation_blob": "%tobacco%"},
    fmt="yaml",
)
print(result)
