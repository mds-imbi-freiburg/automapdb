import pytest
from sqlalchemy.orm import Session

from automapdb.db import AutoMapDB
from automapdb.manager import TableManager
from automapdb.mapper import TableMapper

TEST_CONNECTION_STRING = "postgresql://i2b2:demouser@i2b2-pg:5432/i2b2"
TEST_TABLES_FILE = "tests/tables.json"

pm_cell_data = {
    "path": "i2b2pm.pm_cell_data",
    "get_args": ["cell_id", "project_path"],
    "set_args": ["cell_id", "project_path"],
}

test_user_data = {
    "user": {
        "path": "i2b2pm.pm_user_data",
        "get_args": ["user_id"],
        "set_args": ["user_id", "full_name", "status_cd"],
    }
}
pm_test_cell_data = {"project_path": "/test", "cell_id": "123", "name": None}


@pytest.fixture(scope="module")
def automapdb():
    yield AutoMapDB(TEST_CONNECTION_STRING)


def test_db_connection_should_have_session(automapdb):
    r = automapdb.connection.execute("SELECT 1").fetchone()
    assert automapdb.session.__class__ == Session
    assert r == (1,)


def test_db_with_other_schema_is_same_singleton(automapdb):
    db = AutoMapDB(TEST_CONNECTION_STRING)
    db.set_schema("public")
    automapdb.set_schema("i2b2pm")
    assert db == automapdb


def test_map_proxy_table(automapdb):
    automapdb.set_schema("i2b2pm")
    proxytable = automapdb.get_table("i2b2pm.pm_cell_data")
    assert proxytable.not_nullable() == pm_cell_data["set_args"]
    assert proxytable.primary_keys() == pm_cell_data["get_args"]


def test_table_add_pm_cell_data(automapdb):
    table = TableManager(automapdb)
    try:
        table.delete("i2b2pm.pm_cell_data", "123", "/test")
    except Exception as err:
        print(err)
    table.add("i2b2pm.pm_cell_data", "123", "/test")


def test_table_get_pm_cell_data(automapdb):
    table = TableManager(automapdb)
    r = table.get(
        "i2b2pm.pm_cell_data",
        "123",
        "/test",
        fields=["cell_id", "project_path", "name"],
    )
    assert r == pm_test_cell_data


def test_table_update_pm_cell_data(automapdb):
    table = TableManager(automapdb)
    r = table.update(
        "i2b2pm.pm_cell_data",
        "123",
        "/test",
        "project_path",
        "/test/test2",
    )

    r = table.get(
        "i2b2pm.pm_cell_data",
        "123",
        "/test/test2",
        fields=["project_path"],
    )
    assert r is not None


def test_table_list_pm_cell_data(automapdb):
    table = TableManager(automapdb)
    id_list = table.list("i2b2pm.pm_cell_data", ["cell_id"])
    ids = [i["cell_id"] for i in id_list]
    test_ids = ["CRC", "FRC", "ONT", "WORK", "IM"]
    assert all([True for i in test_ids if i in ids])


def test_table_list_pm_cell_data_with_ilike(automapdb):
    table = TableManager(automapdb)
    result = table.list(
        "i2b2demodata.observation_fact",
        fields=["encounter_num", "observation_blob"],
        ilike={"observation_blob": "%tobacco%"},
    )
    assert len(result) == 1
    assert result[0]["encounter_num"] == 483572


def test_mappings_crud(automapdb):
    t = TableMapper(automapdb)
    t.add_all("i2b2pm")
    t.delete(name="i2b2pm.pm_user_data")
    t.add("i2b2pm.pm_user_data", name="user_table")
    t.update(
        "i2b2pm.pm_user_data",
        name="user",
        get_args=["user_id"],
        set_args=["user_id", "full_name", "status_cd"],
    )
    data = t.show("i2b2pm.pm_user_data")
    assert data == test_user_data


def test_mappings_crud_without_mappings(automapdb):
    t = TableMapper(automapdb, mapping_file=None)
    t.add_all("i2b2pm")
    t.delete(name="i2b2pm.pm_user_data")
    t.add("i2b2pm.pm_user_data", name="user_table")
    t.update(
        "i2b2pm.pm_user_data",
        name="user",
        get_args=["user_id"],
        set_args=["user_id", "full_name", "status_cd"],
    )
    data = t.show("i2b2pm.pm_user_data")
    assert t.mapping_file is None
    assert data == test_user_data


def test_table_del_pm_cell_data(automapdb):
    table = TableManager(automapdb)
    d = table.delete("i2b2pm.pm_cell_data", "123", "/test/test2")
    assert d
