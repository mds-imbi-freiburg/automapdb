from docker

RUN apk update
RUN apk add postgresql postgresql-libs python3 build-base postgresql-dev git
RUN python3 -m ensurepip 
RUN apk add python3-dev
RUN python3 -m pip install psycopg2-binary

ADD . /automapdb
WORKDIR /automapdb

RUN python3 -m pip install -r requirements.dev.txt
RUN python3 -m pip install .

